﻿using GuessingGame.Website.Models.Game;
using GuessingGame.Website.Models.Player;
using GuessingGame.Website.Models.Statistics;

namespace GuessingGame.Website.ViewModels.Home
{
    public class HomeViewModel
    {
        public GameTurnSubmission GameTurnSubmission { get; set; }
        public NewGameSubmission NewGameSubmision { get; set; }
        public GameTurn GameTurn { get; set; }
        public bool ShowNewGameSubbmision { get; set; }
        public bool ShowGame { get; set; }
        public bool ShowStats { get; set; }
        public bool GameIsOver { get; set; }
        public string Title { get; set; }
        public string ErrorMessage { get; set; }
        public string BirthDate { get; set; }
        public string LastGuess { get; set; }
        public TurnCountStatistics TurnCountStatistics { get; set; }
        public TurnSpeedStatistics PlayerTurnSpeedStatistics { get; set; }
        public Player Player { get; set; }
    }
}
