﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Website.Models
{
    public static class Hardcoded
    {
        // change to correct endpoint if needed
        public static string ApiBase => "http://localhost:49930/";
    }
}
