﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Website.Models.Game
{
    public class GameTurnSubmission
    {
        public long GameId { get; set; }
        public List<long> Guesses { get; set; }
    }
}
