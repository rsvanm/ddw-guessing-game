﻿namespace GuessingGame.Website.Models.Statistics
{
    public class TurnSpeedStatistics
    {
        public double MedianTurnTime { get; set; }
        public double AvgTurnTime { get; set; }
        public double MaxTurnTime { get; set; }
        public double MinTurnTime { get; set; }
    }
}
