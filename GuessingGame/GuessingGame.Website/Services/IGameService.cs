﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;

namespace GuessingGame.Website.Services
{
    public interface IGameService
    {
        Task<GameTurn> GetNewGame(NewGameSubmission submission);
        Task<GameTurn> SubmitTurn(GameTurnSubmission submission);
    }
}
