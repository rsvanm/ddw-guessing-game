﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;
using GuessingGame.Website.Models.Statistics;
using GuessingGame.Website.ViewModels.Home;

namespace GuessingGame.Website.Services
{
    public class HomeService: IHomeService
    {
        private readonly IGameService _gameService;
        private readonly IStatisticsService _statisticsService;
        private readonly IPlayerService _playerService;
        private string errormessage;
        private DateTime dateOfBirth;

        public HomeService(IGameService gameService, 
            IStatisticsService statisticsService, 
            IPlayerService playerService)
        {
            _gameService = gameService;
            _statisticsService = statisticsService;
            _playerService = playerService;
            errormessage = string.Empty;
            dateOfBirth = new DateTime();
        }

        public async Task<HomeViewModel> InitViewModel()
        {
            var viewModel = new HomeViewModel
            {
                Title = "Home",
                ShowNewGameSubbmision = true,
            };
            return viewModel;
        }

        public async Task<HomeViewModel> InitGameViewModel(HomeViewModel viewModel, string birthdate)
        {
            if (!IsValid(viewModel.NewGameSubmision, birthdate))
            {
                viewModel.ErrorMessage = $"Invalid submission: {errormessage}";
                viewModel.ShowNewGameSubbmision = true;
                return viewModel;
            }

            var formattedName = viewModel.NewGameSubmision.Name.Replace('\n', ' ').Replace('\t', ' ');
            viewModel.NewGameSubmision.Name = formattedName;
            viewModel.NewGameSubmision.DateOfBirth = dateOfBirth;

            var newGameTurn = await _gameService.GetNewGame(viewModel.NewGameSubmision);
            return new HomeViewModel
            {
                GameTurn = newGameTurn,
                ShowGame = true,
                Title = "New game started",
                Player = await _playerService.GetPlayer(newGameTurn.PlayerId).ConfigureAwait(true),
            };
        }

        public async Task<HomeViewModel> InitGameViewModel(HomeViewModel viewModel)
        {
            if (!IsValid(viewModel.GameTurnSubmission, viewModel.GameTurn.NumSize))
            {
                viewModel.ErrorMessage = $"Invalid submission: {errormessage}";
                viewModel.ShowGame = true;
                return viewModel;
            }

            viewModel.GameTurnSubmission.GameId = viewModel.GameTurn.GameId;

            var gameTurn = await _gameService.SubmitTurn(viewModel.GameTurnSubmission).ConfigureAwait(true);

            if (gameTurn.CorrectGuesses == gameTurn.NumSize)
            {
                return new HomeViewModel
                {
                    LastGuess = string.Join(", ", viewModel.GameTurnSubmission.Guesses),
                    ShowStats = true,
                    Title = "Your game",
                    TurnCountStatistics = await _statisticsService.GetTurnCountStatistics(gameTurn.NumSize).ConfigureAwait(true), 
                    PlayerTurnSpeedStatistics = await _statisticsService.GetPlayerTurnSpeedStatistics(gameTurn.PlayerId).ConfigureAwait(true),
                };
            }

            return new HomeViewModel
            {
                LastGuess = string.Join(", ", viewModel.GameTurnSubmission.Guesses),
                GameTurn = gameTurn,
                ShowGame = true,
                Title = "Your game",
                Player = await _playerService.GetPlayer(gameTurn.PlayerId).ConfigureAwait(true),
                ErrorMessage = $"Invalid submission: {errormessage}",
        };
        }

        private bool IsValid(NewGameSubmission submission, string birthdate)
        {
            if (string.IsNullOrWhiteSpace(birthdate))
            {
                errormessage += " Enter a date in the specified format, e.g. 01/01/1980";
                return false;
            }
            var parts = birthdate.Split('/');
            if (parts.Length < 3)
            {
                errormessage += " Enter a date in the specified format, e.g. 01/01/1980";
                return false;
            }

            try
            {
                dateOfBirth = new DateTime(int.Parse(parts[2]), int.Parse(parts[1]), int.Parse(parts[0]));
            }
            catch (Exception e)
            {
                // to avoid a lot of datetime and tryParse checks 
                return false;

            }

            if (submission.NumSize > 8 || submission.NumSize < 4)
            {
                errormessage += " Enter a number between 4 and 8";
                return false;
            }
            if (submission.NumSize > 8 || submission.NumSize < 4)
            {
                errormessage += " Enter a number between 4 and 8";
                return false;
            }
            if (string.IsNullOrWhiteSpace(submission.Name))
            {
                errormessage += " Enter a name";
                return false;
            }

            return true;
        }

        private bool IsValid(GameTurnSubmission submission, long requiredSize)
        {
            long guessNum = 0;
            foreach (var num in submission.Guesses)
            {
                guessNum += 1;
                if (num < 0 || num > 10)
                {
                    errormessage += " Invalid number. Enter a number 1-10";
                    return false;
                }
            }

            if (guessNum != requiredSize)
            {
                errormessage += $" Enter correct number of guesses ({requiredSize})";
                return false;
            }
            return true;
        }
    }
}
