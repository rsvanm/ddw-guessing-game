﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Statistics;
using GuessingGame.Website.Repositories;

namespace GuessingGame.Website.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly IStatisticsRepository _statisticsRepository;
        public StatisticsService(IStatisticsRepository statisticsRepository)
        {
            _statisticsRepository = statisticsRepository;
        }

        public async Task<TurnCountStatistics> GetTurnCountStatistics(long grouping)
        {
            return await _statisticsRepository.GetTurnCountStatistics(grouping);
        }

        public async Task<TurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId)
        {
            return await _statisticsRepository.GetPlayerTurnSpeedStatistics(playerId);
        }
    }
}
