﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Player;
using GuessingGame.Website.Repositories;

namespace GuessingGame.Website.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly IPlayerRepository _playerRepository;

        public PlayerService(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<Player> GetPlayer(long playerId)
        {
            return await _playerRepository.GetPlayer(playerId);
        }
    }
}
