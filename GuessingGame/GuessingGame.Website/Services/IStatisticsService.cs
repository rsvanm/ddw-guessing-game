﻿using System.Threading.Tasks;
using GuessingGame.Website.Models.Statistics;

namespace GuessingGame.Website.Services
{
    public interface IStatisticsService
    {
        Task<TurnCountStatistics> GetTurnCountStatistics(long grouping);
        Task<TurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId);
    }
}
