﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;
using GuessingGame.Website.ViewModels.Home;

namespace GuessingGame.Website.Services
{
    public interface IHomeService
    {
        Task<HomeViewModel> InitViewModel();
        Task<HomeViewModel> InitGameViewModel(HomeViewModel submission);
        Task<HomeViewModel> InitGameViewModel(HomeViewModel submission, string birthdate);
    }
}
