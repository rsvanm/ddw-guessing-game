﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;
using GuessingGame.Website.Repositories;

namespace GuessingGame.Website.Services
{
    public class GameService : IGameService
    {
        private readonly IGameRepository _repository;
        public GameService(IGameRepository repository)
        {
            _repository = repository;
        }
        public async Task<GameTurn> GetNewGame(NewGameSubmission submission)
        {
            return await _repository.GetNewGame(submission).ConfigureAwait(true);
        }

        public async Task<GameTurn> SubmitTurn(GameTurnSubmission submission)
        {
            return await _repository.GetNewTurn(submission);
        }
    }
}
