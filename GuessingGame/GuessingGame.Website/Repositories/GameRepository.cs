﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;
using Newtonsoft.Json;

namespace GuessingGame.Website.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly HttpClient _httpClient;
        public GameRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<GameTurn> GetNewGame(NewGameSubmission submission)
        {
            var content = new StringContent(JsonConvert.SerializeObject(submission), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("api/games/getnewgame", content).ConfigureAwait(true);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GameTurn>(json);
        }

        public async Task<GameTurn> GetNewTurn(GameTurnSubmission submission)
        {
            var content = new StringContent(JsonConvert.SerializeObject(submission), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("api/games/getnewturn", content).ConfigureAwait(true);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GameTurn>(json);
        }
    }
}
