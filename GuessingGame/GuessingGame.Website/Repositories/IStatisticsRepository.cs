﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Statistics;

namespace GuessingGame.Website.Repositories
{
    public interface IStatisticsRepository
    {
        Task<TurnCountStatistics> GetTurnCountStatistics(long grouping);
        Task<TurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId);
    }
}
