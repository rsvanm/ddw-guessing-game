﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Player;

namespace GuessingGame.Website.Repositories
{
    public interface IPlayerRepository
    {
        Task<Player> GetPlayer(long playerId);
    }
}
