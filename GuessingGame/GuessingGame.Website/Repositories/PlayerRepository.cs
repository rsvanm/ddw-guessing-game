﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;
using GuessingGame.Website.Models.Player;
using Newtonsoft.Json;

namespace GuessingGame.Website.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly HttpClient _httpClient;

        public PlayerRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Player> GetPlayer(long playerId)
        {  
            var response = await _httpClient.GetAsync($"api/player/getplayer/{playerId}").ConfigureAwait(true);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Player>(json);  
        }
    }
}
