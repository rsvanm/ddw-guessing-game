﻿using System.Net.Http;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Statistics;
using Newtonsoft.Json;

namespace GuessingGame.Website.Repositories
{
    public class StatisticsRepository : IStatisticsRepository
    {
        private readonly HttpClient _httpClient;

        public StatisticsRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<TurnCountStatistics> GetTurnCountStatistics(long grouping)
        {
            var response = await _httpClient.GetAsync(string.Format("api/statistics/getturncountstatistics/{0}", grouping.ToString())).ConfigureAwait(true);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TurnCountStatistics>(json);
        }

        public async Task<TurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId)
        {
            var response = await _httpClient.GetAsync(string.Format("api/statistics/getplayerturnspeedstatistics/{0}", playerId.ToString())).ConfigureAwait(true);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TurnSpeedStatistics>(json);
        }
    }
}
