﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Website.Models.Game;

namespace GuessingGame.Website.Repositories
{
    public interface IGameRepository
    {
        Task<GameTurn> GetNewGame(NewGameSubmission submission);
        Task<GameTurn> GetNewTurn(GameTurnSubmission submission);
    }
}
