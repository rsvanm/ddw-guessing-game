﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using GuessingGame.Website.Models;
using GuessingGame.Website.Repositories;
using GuessingGame.Website.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GuessingGame.Website
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var apiEndpoint = new Uri(Hardcoded.ApiBase); // httpclient endpoint
            var httpClient = new HttpClient
            {
                BaseAddress = apiEndpoint,
            };
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            services.AddTransient<IGameService, GameService>();
            services.AddTransient<IHomeService, HomeService>();
            services.AddTransient<IPlayerService, PlayerService>();
            services.AddTransient<IGameRepository, GameRepository>();
            services.AddTransient<IPlayerRepository, PlayerRepository>();
            services.AddTransient<IStatisticsService, StatisticsService>();
            services.AddTransient<IStatisticsRepository, StatisticsRepository>();
            services.AddSingleton(httpClient);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
