﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GuessingGame.Website.Models;
using GuessingGame.Website.Models.Game;
using GuessingGame.Website.Services;
using GuessingGame.Website.ViewModels.Home;

namespace GuessingGame.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = await _homeService.InitViewModel().ConfigureAwait(true);
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> NewGame(HomeViewModel submission)
        {
            var viewModel = await _homeService.InitGameViewModel(submission, submission.BirthDate).ConfigureAwait(true);
            return View("Index",viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> NewTurn(HomeViewModel submission)
        {
            var viewModel = await _homeService.InitGameViewModel(submission).ConfigureAwait(true);
            return View("Index", viewModel);
        }
    }
}
