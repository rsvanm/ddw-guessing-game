﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.LogFile;
using Newtonsoft.Json;

namespace GuessingGame.Api.Builders
{
    public class LogLineBuilder : ILogLineBuilder
    {
        public string BuildLogLine(LogFileItem logFileItem)
        {
            if (!logFileItem.Guesses.Any())
                Populate(logFileItem, ListEnum.Guesses);
            if (!logFileItem.RequiredOutcome.Any())
                Populate(logFileItem, ListEnum.RequiredOutcome);

            var guesses = string.Join(',', logFileItem.Guesses);
            var requiredOutCome = string.Join(',', logFileItem.RequiredOutcome);

            var sb = new StringBuilder();
            sb.Append(logFileItem.GameId.ToString());
            sb.Append("\t");
            sb.Append(logFileItem.PlayerId.ToString());
            sb.Append("\t");
            sb.Append(logFileItem.NumSize.ToString());
            sb.Append("\t");
            sb.Append(guesses);
            sb.Append("\t");
            sb.Append(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                CultureInfo.InvariantCulture));
            sb.Append("\t");
            sb.Append(logFileItem.DateOfBirth.ToShortDateString());
            sb.Append("\t");
            sb.Append(logFileItem.Name);
            sb.Append("\t");
            sb.Append(requiredOutCome);
            sb.Append("\n");

            return sb.ToString();
        }

        #region HelperMethods

        private static void Populate(LogFileItem logFileItem, ListEnum type)
        {
            for (var i = 0; i < logFileItem.NumSize; i++)
            {
                switch (type)
                {
                    case ListEnum.Guesses:
                        logFileItem.Guesses.Add(-1);
                        break;
                    case ListEnum.RequiredOutcome:
                        var rnd = new Random();
                        logFileItem.RequiredOutcome.Add(rnd.Next(1,10));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(type), type, null);
                }
            }
        }

        #endregion
    }
}

