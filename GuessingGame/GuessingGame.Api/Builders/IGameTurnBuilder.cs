﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;

namespace GuessingGame.Api.Builders
{
    public interface IGameTurnBuilder
    {
        GameTurn BuildGameTurn(LogFile logFile, GameTurnSubmission submission);
        GameTurn BuildGameTurn(Tuple<long, long> ids, NewGameSubmission submission);
    }
}
