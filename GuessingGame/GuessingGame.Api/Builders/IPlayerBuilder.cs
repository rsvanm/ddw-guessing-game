﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Model.Player;

namespace GuessingGame.Api.Builders
{
    public interface IPlayerBuilder
    {
        Player BuildPlayer(LogFileItem logFile);
    }
}
