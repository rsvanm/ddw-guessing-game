﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.LogFile;

namespace GuessingGame.Api.Builders
{
    public interface ILogLineBuilder
    {
        string BuildLogLine(LogFileItem logFileItem);
    }
}
