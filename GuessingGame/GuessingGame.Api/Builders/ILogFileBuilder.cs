﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;
using IList = GuessingGame.Api.Custom.IList;

namespace GuessingGame.Api.Builders
{
    public interface ILogFileBuilder
    {
        LogFile BuildLogFile<T>(string logfileText, Dictionary<LogOptionsEnum, LogOptionsValues<T>> options);
        Tuple<long, long> GetNewGameIds(string logfileText, NewGameSubmission submission);
        LogFileItem GetSinglePlayerLogfile(string logfileText, long playerId);
    }
}
