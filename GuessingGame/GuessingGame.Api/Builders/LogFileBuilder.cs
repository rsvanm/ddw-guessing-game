﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace GuessingGame.Api.Builders
{
    public class LogFileBuilder : ILogFileBuilder
    {
        private readonly LogFile logFile;

        public LogFileBuilder()
        {
            logFile = new LogFile {Items = new List<LogFileItem>()};
        }

        public LogFile BuildLogFile<T>(string logfileText, Dictionary<LogOptionsEnum, LogOptionsValues<T>> options)
        {
            var lines = GetLines(logfileText);
            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                var data = GetData(line).ToList();

                // check if line contains required number of items
                if (!data.Any() || data.Count() < 8)
                    continue;

                // try parsing gameId from line
                if (!long.TryParse(data[0].Trim(), out var parsedGameId))
                    continue;

                long gameId = -1; // set to default value, used for later checks
                // if a gameId is passed, filter non-matching gameIds
                if (options != null && options.TryGetValue(LogOptionsEnum.Game, out var gameIds)
                    && long.TryParse(gameIds[0].ToString(), out gameId) && gameId!= parsedGameId)
                    continue;

                // try parsing playerId from line
                if (!long.TryParse(data[1].Trim(), out var parsedPlayerId))
                    continue;

                // if a playerId is passed, filter non-matching gameIds
                if (options != null && options.TryGetValue(LogOptionsEnum.Player, out var playerIds)
                    && long.TryParse(playerIds[0].ToString(), out var playerId) && playerId != parsedPlayerId)
                    continue;

                // check if input is in the required format
                if (!long.TryParse(data[2].Trim(), out var numSize) || !DateTime.TryParse(data[4].Trim(),
                                                                        out var timeStamp)
                                                                    || !DateTime.TryParse(data[5].Trim(),
                                                                        out var dateOfBirth))
                    continue;

                var guesses = data[3].Trim().Split(',');
                var requiredOutput = data[7].Trim().Split(',');
                // check if number of guesses matches required number
                if (guesses.Length != numSize || requiredOutput.Length != numSize)
                    continue;

                //on parse error, assign value as -1
                var parsedGuesses = guesses.Select(n =>
                        long.TryParse(n, out var i) ? i : -1)
                    .ToList();
                var parsedRequiredOutput = requiredOutput.Select(n =>
                        long.TryParse(n, out var i) ? i : -1)
                    .ToList();

                // check if any of the values equal -1, signaling a parse error
                // skip check on new turn submission (positive gameId is passed as argument)
                if (parsedGuesses.Any(p => p == -1) && gameId == -1 || parsedRequiredOutput.Any(p => p == -1))
                    continue;

                // line is valid
                logFile.Items.Add(new LogFileItem
                {
                    GameId = parsedGameId,
                    PlayerId = parsedPlayerId,
                    NumSize = numSize,
                    Guesses = parsedGuesses,
                    TimeStamp = timeStamp,
                    DateOfBirth = dateOfBirth,
                    Name = data[6].Trim(),
                    RequiredOutcome = parsedRequiredOutput,
                });
            }

            return logFile;
        }

        // TODO: refactor
        // Function should be split:
        // - LogFileBuilder.BuildLogFile with new options to retrieve last line in file
        // - GameService.GetNewGame to select and increment ids from retrieved line
        public Tuple<long, long> GetNewGameIds(string logfileText, NewGameSubmission submission)
        {
            long maxGameId = -1;
            long maxPlayerId = -1;
            long playerId = 0;
            var playerIsNew = true;
            var lines = GetLines(logfileText);
            foreach (var line in lines)
            {
                var data = GetData(line).ToList();
                if (data.Count() < 8)
                    continue;

                // no gameId set
                if (string.IsNullOrWhiteSpace(data[0].Trim()))
                    continue;
                long.TryParse(data[0].Trim(), out maxGameId);

                // check if playerId is set
                if (string.IsNullOrWhiteSpace(data[1].Trim()))
                    continue;
                long.TryParse(data[1].Trim(), out maxPlayerId);

                // no date of birth/name set
                if (string.IsNullOrWhiteSpace(data[6]) || string.IsNullOrWhiteSpace(data[5]))
                    continue;
                // check if player exists
                if (!data[6].Trim().Equals(submission.Name.Trim()) ||
                    !data[5].Trim().Equals(submission.DateOfBirth.ToShortDateString()))
                    continue;

                playerIsNew = false;
                long.TryParse(data[1].Trim(), out playerId);
            }

            var ids = playerIsNew
                ? Tuple.Create(maxGameId + 1, maxPlayerId + 1)
                : Tuple.Create(maxGameId + 1, playerId);

            return ids;
        }

        public LogFileItem GetSinglePlayerLogfile(string logfileText, long playerId)
        {
            long parsedPlayerId = 0;
            var dateOfBirth = new DateTime();
            var name = string.Empty;
            
            var lines = GetLines(logfileText);
            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                var data = GetData(line).ToList();
                if (data.Count() < 8)
                    continue;

                // try parsing playerId from line
                if (!long.TryParse(data[1].Trim(), out parsedPlayerId))
                    continue;

                // match playerId
                if (parsedPlayerId != playerId)
                    continue;

                // try parsing date of birth
                if (!DateTime.TryParse(data[5].Trim(), out dateOfBirth))
                    continue;

                var guesses = data[3].Trim().Split(',').Length;

                if (!string.IsNullOrWhiteSpace(data[6].Trim()))
                {
                    name = data[6].Trim();
                    return new LogFileItem
                    {
                        GameId = long.TryParse(data[0], out var gameId) ? gameId : 0,
                        PlayerId = parsedPlayerId,
                        NumSize = 0,
                        Guesses = new List<long>(),
                        TimeStamp = new DateTime(),
                        DateOfBirth = dateOfBirth,
                        Name = name,
                        RequiredOutcome = new List<long>(),
                    };
                }
            }

            throw new GameApiException(nameof(LogFileItem));
        }
    

    #region HelperMethods
        private static IEnumerable<string> GetLines(string logfileText)
        {
            return logfileText.Split('\n');
        }

        private static IEnumerable<string> GetData(string line)
        {
            return line.Split('\t');
        }
        #endregion
    }
}
