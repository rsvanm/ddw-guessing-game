﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Model.Statistics;

namespace GuessingGame.Api.Builders
{
    public class StatisticsBuilder : IStatisticsBuilder
    {
        public TurnCountStatistics BuildTurnCountStatistics(LogFile logFile, long grouping)
        {
            if (logFile.Items == null || !logFile.Items.Any())
                throw new GameApiException(nameof(logFile.Items));

            var turnsCounter = new List<long>();
            var groupedByGameId = logFile.Items.Where(c => c.NumSize == grouping).GroupBy(x => x.GameId);
            foreach (var group in groupedByGameId)
            {
                var logFileItems = group.ToList();
                // only count succesful games
                var lastItem = logFileItems.Last();
                var c = 0;
                for (var i = 0; i < lastItem.RequiredOutcome.Count; i++)
                {
                    if (lastItem.RequiredOutcome.ToList()[i] == lastItem.Guesses.ToList()[i])
                    {
                        c += 1;
                    }
                }

                if (c == lastItem.NumSize)
                    turnsCounter.Add(logFileItems.Count);
            }

            return new TurnCountStatistics
            {
                NumTurns = grouping,
                Min = GetMin(turnsCounter),
                Max = GetMax(turnsCounter),
                Median = GetMedian(turnsCounter),
                Average = GetAverage(turnsCounter),
            };
        }

        public PlayerTurnSpeedStatistics BuildPlayerTurnSpeedStatistics(LogFile logFile)
        {
            if (logFile.Items == null || !logFile.Items.Any())
                throw new GameApiException(nameof(logFile.Items));

            var groupedByGameId = logFile.Items.GroupBy(x => x.GameId);
            var gameStatisticsList = new List<GameTurnSpeedStatistics>();
            foreach (var group in groupedByGameId)
            {
                var items = group.ToList();
                
                if (items.Count > 0)
                    gameStatisticsList.Add(GetGameStatistics(new LogFile { Items = items }));
            }

            var playerStats = new PlayerTurnSpeedStatistics
            {
                PlayerId = logFile.Items.FirstOrDefault().PlayerId,
                AvgTurnTime = GetAverage(gameStatisticsList.Select(x => x.AvgTurnTime).ToList()),
                MedianTurnTime = GetMedian(gameStatisticsList.Select(x => x.MedianTurnTime).ToList()),
                MinTurnTime = GetMin(gameStatisticsList.Select(x => x.MinTurnTime).ToList()),
                MaxTurnTime = GetMax(gameStatisticsList.Select(x => x.MaxTurnTime).ToList()),
            };

            return playerStats;
        }

        private GameTurnSpeedStatistics GetGameStatistics(LogFile logFile)
        {
            if (logFile.Items == null || !logFile.Items.Any())
                throw new GameApiException(nameof(logFile.Items));

            // first try all numbers correct
            if (logFile.Items.Count == 1)
            return new GameTurnSpeedStatistics
            {
                AvgTurnTime = 0,
                MaxTurnTime = 0,
                MinTurnTime = 0,
                MedianTurnTime = 0,
            };

            var differences = new List<double>();
            for (var i = 0; i < logFile.Items.Count-1; i++)
            {
                differences.Add((logFile.Items[i+1].TimeStamp - logFile.Items[i].TimeStamp).TotalMilliseconds);
            }

            return new GameTurnSpeedStatistics
            {
                AvgTurnTime = GetAverage(differences),
                MedianTurnTime = GetMedian(differences),
                MinTurnTime = GetMin(differences),
                MaxTurnTime = GetMax(differences),
            };
        }

        #region HelperMethods

        internal double GetAverage(ICollection<long> nums)
        {
            if (!nums.Any())
                return 0;

            return Math.Round(nums.Average(), 2);
        }

        internal double GetAverage(ICollection<double> nums)
        {
            if (!nums.Any())
                return 0;

            return Math.Round(nums.Average(), 2);
        }

        internal double GetMedian(ICollection<long> nums)
        {
            if (!nums.Any())
                return 0;

            if (nums.Count == 1)
                return nums.ToList()[0];

            nums.ToList().Sort();
           var sortedList = nums.ToList();
           var centerVal = sortedList.Count/2;

           return centerVal % 2 == 0 ? 
               ((double)sortedList[centerVal] + sortedList[centerVal - 1]) /2 :
                sortedList[centerVal];
        }

        internal double GetMedian(ICollection<double> nums)
        {
            if (!nums.Any())
                return 0;

            if (nums.Count == 1)
                return nums.ToList()[0];

            nums.ToList().Sort();
            var sortedList = nums.ToList();
            var centerVal = sortedList.Count / 2;

            return centerVal % 2 == 0 ?
                ((double)sortedList[centerVal] + sortedList[centerVal - 1]) / 2 :
                sortedList[centerVal];
        }

        internal T GetMin<T>(ICollection<T> nums)
            where T : new()
        {
            var n = new T();
            if (!nums.Any())
                return n;

            return nums.Min();
        }

        internal T GetMax<T>(ICollection<T> nums)
            where T : new()
        {
            var n = new T();
            if (!nums.Any())
                return n;
            return nums.Max();
        }
        #endregion
    }
}
