﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Model.Statistics;

namespace GuessingGame.Api.Builders
{
    public interface IStatisticsBuilder
    {
        TurnCountStatistics BuildTurnCountStatistics(LogFile logFile, long grouping);
        PlayerTurnSpeedStatistics BuildPlayerTurnSpeedStatistics(LogFile logFile);
    }
}
