﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Model.Player;

namespace GuessingGame.Api.Builders
{
    public class PlayerBuilder : IPlayerBuilder
    {
        public Player BuildPlayer(LogFileItem playerdata)
        {

            return new Player
            {
                PlayerId = playerdata.PlayerId,
                Age = GetAge(playerdata.DateOfBirth),
                Name = playerdata.Name,
            };
        }

        #region HelperMethods

        public long GetAge(DateTime dateOfBirth)
        {
            var now = DateTime.Now;
            var age = now.Year - dateOfBirth.Year;
            if (now.Month < dateOfBirth.Month || (now.Month == dateOfBirth.Month && now.Day < dateOfBirth.Day))
                age--;

            return age;
        }

        #endregion

    }
}
