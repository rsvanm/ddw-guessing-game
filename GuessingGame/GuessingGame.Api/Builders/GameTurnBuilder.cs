﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;

namespace GuessingGame.Api.Builders
{
    public class GameTurnBuilder : IGameTurnBuilder
    {
        public GameTurn BuildGameTurn(Tuple<long, long> ids, NewGameSubmission submission)
        {
            return new GameTurn
            {
                GameId = ids.Item1,
                PlayerId = ids.Item2,
                CorrectGuesses = 0,
                NumSize = submission.NumSize,
                TurnNumber = 0,
            };
        }

        public GameTurn BuildGameTurn(LogFile logFile, GameTurnSubmission submission)
        {
            var correctGuesses = 0;
            var logFileItem = logFile.Items.FirstOrDefault();
            var requiredOutcome = logFileItem.RequiredOutcome.ToList();
            for (var i = 0; i < requiredOutcome.Count;  i++)
            {
                if (requiredOutcome[i] == submission.Guesses.ToList()[i])
                    correctGuesses += 1;

            }
            return new GameTurn
            {
                CorrectGuesses = correctGuesses,
                GameId = logFileItem.GameId,
                NumSize = logFileItem.NumSize,
                TurnNumber = logFile.Items.Count,
                PlayerId = logFileItem.PlayerId,
            };
        }
    }
}
