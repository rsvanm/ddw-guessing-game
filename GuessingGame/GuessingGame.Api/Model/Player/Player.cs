﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Player
{
    public class Player
    {
        public long PlayerId { get; set; }
        public long Age { get; set; }
        public string Name { get; set; }
    }
}
