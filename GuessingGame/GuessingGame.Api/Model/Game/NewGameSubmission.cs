﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Game
{
    public class NewGameSubmission
    {
        public string Name { get; set; }
        public long NumSize { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
