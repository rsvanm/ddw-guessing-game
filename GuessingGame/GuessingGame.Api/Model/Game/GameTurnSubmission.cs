﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Game
{
    public class GameTurnSubmission
    {
        public long GameId { get; set; }
        public ICollection<long> Guesses { get; set; }
    }
}
