﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Game
{
    public class GameTurn
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public long NumSize { get; set; }
        public long CorrectGuesses { get; set; }
        public long TurnNumber { get; set; }
    }
}
