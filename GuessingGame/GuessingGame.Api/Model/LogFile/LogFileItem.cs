﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.LogFile
{
    public class LogFileItem
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public long NumSize { get; set; }
        public ICollection<long> Guesses { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Name { get; set; }
        public ICollection<long> RequiredOutcome { get; set; }
    }
}
