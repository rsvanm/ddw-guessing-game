﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.LogFile
{
    public class LogFile
    {
        public List<LogFileItem> Items { get; set; }
    }
}
