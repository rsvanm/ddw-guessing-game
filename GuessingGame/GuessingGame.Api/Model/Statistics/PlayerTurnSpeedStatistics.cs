﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Statistics
{
    public class PlayerTurnSpeedStatistics : TurnSpeedStatistics
    {
        public long PlayerId { get; set; }
    }
}
