﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Statistics
{
    public class TurnSpeedStatistics
    {
        public double MedianTurnTime { get; set; }
        public double AvgTurnTime { get; set; }
        public double MaxTurnTime { get; set; }
        public double MinTurnTime { get; set; }
    }
}
