﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Statistics
{
    public class GameTurnSpeedStatistics : TurnSpeedStatistics
    {
        public long GameId { get; set; }
    }
}
