﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Model.Statistics
{
    public class TurnCountStatistics
    {
        public long NumTurns { get; set; }
        public long Min { get; set; }
        public long Max { get; set; }
        public double Median { get; set; }
        public double Average { get; set; }
    }
}
