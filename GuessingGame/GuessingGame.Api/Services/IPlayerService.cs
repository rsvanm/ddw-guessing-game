﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.Player;

namespace GuessingGame.Api.Services
{
    public interface IPlayerService
    {
        Task<Player> GetPlayer(long playerId);
    }
}
