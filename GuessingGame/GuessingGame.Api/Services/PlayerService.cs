﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Model.Player;
using GuessingGame.Api.Repositories;

namespace GuessingGame.Api.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly IPlayerBuilder _playerBuilder;
        private readonly ILogfileRepository _logFileRepository;
        private readonly ILogFileBuilder _logFileBuilder;

        public PlayerService(ILogfileRepository logFileRepository, IPlayerBuilder playerBuilder, ILogFileBuilder logFileBuilder)
        {
            _logFileRepository = logFileRepository;
            _playerBuilder = playerBuilder;
            _logFileBuilder = logFileBuilder;
        }

        public async Task<Player> GetPlayer(long playerId)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var logFile = _logFileBuilder.GetSinglePlayerLogfile(logFileText, playerId);
            return _playerBuilder.BuildPlayer(logFile);
        }
    }
}
