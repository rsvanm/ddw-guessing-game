﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.Statistics;
using GuessingGame.Api.Repositories;

namespace GuessingGame.Api.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly ILogfileRepository _logFileRepository;
        private readonly IStatisticsBuilder _statisticsBuilder;
        private readonly ILogFileBuilder _logFileBuilder;

        public StatisticsService(ILogfileRepository logFileRepository, 
            IStatisticsBuilder statisticsBuilder, 
            ILogFileBuilder logFileBuilder)
        {
            _logFileRepository = logFileRepository;
            _statisticsBuilder = statisticsBuilder;
            _logFileBuilder = logFileBuilder;
        }

        public async Task<TurnCountStatistics> GetTurnCountStatistics(long grouping)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var logFile = _logFileBuilder.BuildLogFile<long>(logFileText, null);
            return _statisticsBuilder.BuildTurnCountStatistics(logFile, grouping);
        }

        public async Task<PlayerTurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var options = new Dictionary<LogOptionsEnum, LogOptionsValues<long>>
            {
                [LogOptionsEnum.Player] = new LogOptionsValues<long> { playerId }
            };
            var logFile = _logFileBuilder.BuildLogFile(logFileText, options);
  
            return _statisticsBuilder.BuildPlayerTurnSpeedStatistics(logFile);
        }
    }
}
