﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Repositories;

namespace GuessingGame.Api.Services
{
    public class GameService : IGameService
    {
        private readonly ILogfileRepository _logFileRepository;
        private readonly ILogFileBuilder _logFileBuilder;
        private readonly ILogLineBuilder _logLineBuilder;
        private readonly IGameTurnBuilder _gameTurnBuilder;

        public GameService(ILogfileRepository logFileRepository, 
            ILogFileBuilder logFileBuilder, 
            ILogLineBuilder logLineBuilder, 
            IGameTurnBuilder gameTurnBuidler)
        {
            _logFileRepository = logFileRepository;
            _logFileBuilder = logFileBuilder;
            _logLineBuilder = logLineBuilder;
            _gameTurnBuilder = gameTurnBuidler;
        }
        public async Task AddLogItem(LogFileItem item)
        {
            var line = _logLineBuilder.BuildLogLine(item);
            await _logFileRepository.AddLogLine(line);
        }

        public async Task<LogFile> GetGameLog(long gameId)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var options = new Dictionary<LogOptionsEnum, LogOptionsValues<long>>
                {
                    [LogOptionsEnum.Game] = new LogOptionsValues<long> { gameId }
                };
            return _logFileBuilder.BuildLogFile(logFileText, options);
        }

        public async Task<GameTurn> GetNewGame(NewGameSubmission submission)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var ids = _logFileBuilder.GetNewGameIds(logFileText, submission);
            var logFileLine = _logLineBuilder.BuildLogLine(new LogFileItem
            {
                GameId = ids.Item1,
                PlayerId = ids.Item2,
                Name = submission.Name,
                DateOfBirth = submission.DateOfBirth,
                Guesses = new List<long>(),
                NumSize = submission.NumSize,
                TimeStamp = DateTime.Now,
                RequiredOutcome = new List<long>(),
            });

            await _logFileRepository.AddLogLine(logFileLine).ConfigureAwait(true);
            return _gameTurnBuilder.BuildGameTurn(ids, submission);
        }

        public async Task<GameTurn> GetNewTurn(GameTurnSubmission submission)
        {
            var logFileText = await _logFileRepository.GetLog().ConfigureAwait(true);
            var options = new Dictionary<LogOptionsEnum, LogOptionsValues<long>>
            {
                [LogOptionsEnum.Game] = new LogOptionsValues<long> { submission.GameId }
            };
            var logFile = _logFileBuilder.BuildLogFile(logFileText, options);
            if (!logFile.Items.Any())
                throw new GameApiException(nameof(logFile.Items));

            var item = logFile.Items.FirstOrDefault();
            var logFileLine = _logLineBuilder.BuildLogLine(new LogFileItem
            {
                GameId = item.GameId,
                PlayerId = item.PlayerId,
                Name = item.Name,
                DateOfBirth = item.DateOfBirth,
                Guesses = submission.Guesses,
                NumSize = item.NumSize,
                TimeStamp = DateTime.Now,
                RequiredOutcome = item.RequiredOutcome,
            });

            await _logFileRepository.AddLogLine(logFileLine).ConfigureAwait(false);

            return _gameTurnBuilder.BuildGameTurn(logFile, submission);
        }
    }
}
