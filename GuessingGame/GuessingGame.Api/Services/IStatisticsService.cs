﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Model.Statistics;

namespace GuessingGame.Api.Services
{
    public interface IStatisticsService
    {
        Task<PlayerTurnSpeedStatistics> GetPlayerTurnSpeedStatistics(long playerId);
        Task<TurnCountStatistics> GetTurnCountStatistics(long grouping);
    }
}
