﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;

namespace GuessingGame.Api.Services
{
    public interface IGameService
    {
        Task<LogFile> GetGameLog(long gameId);
        Task AddLogItem(LogFileItem item);
        Task<GameTurn> GetNewGame(NewGameSubmission submission);
        Task<GameTurn> GetNewTurn(GameTurnSubmission submission);
    }
}
