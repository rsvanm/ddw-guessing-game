﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GuessingGame.Api.Controllers
{
    [Route("api/games")]
    public class GamesController : Controller
    {
        private readonly IGameService _gameService;
        public GamesController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpPost]
        [Route("getnewgame")]
        public async Task<IActionResult> GetNewGame([FromBody] NewGameSubmission submission)
        {
            if (ModelState.IsValid)
            {
                return Ok(await _gameService.GetNewGame(submission).ConfigureAwait(false));
            }
            return StatusCode(400, nameof(ControllerContext.ActionDescriptor.ActionName));
        }

        [HttpPost]
        [Route("getnewturn")]
        public async Task<IActionResult> GetNewTurn([FromBody] GameTurnSubmission submission)
        {
            if (ModelState.IsValid)
            {
                return Ok(await _gameService.GetNewTurn(submission).ConfigureAwait(false));
            }
            return StatusCode(400, nameof(ControllerContext.ActionDescriptor.ActionName));
        }
    }
}
