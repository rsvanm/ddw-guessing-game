﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Enums;
using GuessingGame.Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GuessingGame.Api.Controllers
{
    [Route("api/statistics/")]
    public class StatisticsController : Controller
    {
        private readonly IStatisticsService _statisticsService;

        public StatisticsController(IStatisticsService statisticsService)
        {
            _statisticsService = statisticsService;
        }

        [HttpGet]
        [Route("getplayerturnspeedstatistics/{playerId}")]
        public async Task<IActionResult> BuildPlayerTurnSpeedStatistics(long playerId)
        {
            if (ModelState.IsValid)
            {
                return Ok(await _statisticsService.GetPlayerTurnSpeedStatistics(playerId).ConfigureAwait(true));
            }
            return StatusCode(400, nameof(ControllerContext.ActionDescriptor.ActionName));
        }

        [HttpGet]
        [Route("getturncountstatistics/{grouping}")]
        public async Task<IActionResult> GetTurnCountStatistics(long grouping)
        {
            return Ok(await _statisticsService.GetTurnCountStatistics(grouping).ConfigureAwait(true));
        }
    }
}
