﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GuessingGame.Api.Controllers
{
    [Route("api/player/")]
    public class PlayerController : Controller
    {
        private readonly IPlayerService _playerService;

        public PlayerController(IPlayerService playerService)
        {
            _playerService = playerService;
        }

        [HttpGet]
        [Route("getplayer/{playerId}")]
        public async Task<IActionResult> GetPlayer(long playerId)
        {
            return Ok(await _playerService.GetPlayer(playerId).ConfigureAwait(false));
        }

    }
}
