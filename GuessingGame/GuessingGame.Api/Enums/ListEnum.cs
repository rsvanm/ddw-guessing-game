﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Enums
{
    public enum ListEnum
    {
        Guesses,
        RequiredOutcome,
    }
}
