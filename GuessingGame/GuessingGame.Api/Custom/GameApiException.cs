﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessingGame.Api.Custom
{
    public class GameApiException: Exception
    {
        public GameApiException()
            : base() { }

        public GameApiException(string message)
            : base(message) { }

        public GameApiException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public GameApiException(string message, Exception innerException)
            : base(message, innerException) { }

        public GameApiException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }
    
    }
}
