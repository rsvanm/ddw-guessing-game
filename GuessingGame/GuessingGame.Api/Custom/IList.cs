﻿using System.Collections;

namespace GuessingGame.Api.Custom
{
    public interface IList
    {
        void Add(object item);
    }
}
