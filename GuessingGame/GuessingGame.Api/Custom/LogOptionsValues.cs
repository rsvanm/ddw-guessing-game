﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Enums;

namespace GuessingGame.Api.Custom
{
    public class LogOptionsValues<T> : List<T>, IList
    {
        public void AddItem(object item, LogOptionsEnum option)
        {
            switch (option)
            {
                case LogOptionsEnum.Player:
                    if (typeof(object) != typeof(long))
                        return;
                    break;
                case LogOptionsEnum.Game:
                    if (typeof(object) != typeof(long))
                        return;
                    break;
                case LogOptionsEnum.LastLine:
                    if (typeof(object) != typeof(bool))
                        return;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }
            base.Add((T)item);
        }

        public void Add(object item)
        {
            base.Add((T) item);
        }
    }
}
