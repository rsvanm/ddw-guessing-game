﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model.LogFile;

namespace GuessingGame.Api.Repositories
{
    public interface ILogfileRepository
    {
        Task<string> GetLog();
        Task AddLogLine(string logline);
    }
}
