﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GuessingGame.Api.Model;
using GuessingGame.Api.Model.LogFile;
using Microsoft.Extensions.Configuration;

namespace GuessingGame.Api.Repositories
{
    public class LogFileRepository : ILogfileRepository
    {
        private static string path;

        public LogFileRepository()
        {
            path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, HardCoded.LogFile);
        }

        public async Task AddLogLine(string logFileLine)
        {
            if (!File.Exists(path))
                await File.WriteAllTextAsync(path, logFileLine);
            else
                await File.AppendAllTextAsync(path, logFileLine);      
        }

        public async Task<string> GetLog()
        {
            if (!File.Exists(path))
                return string.Empty;

            return await File.ReadAllTextAsync(path);
        }
    }
}
