﻿using System;
using System.Collections.Generic;
using System.Text;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Model.Player;
using Xunit;

namespace GuessingGame.ApiTest
{
    public class PlayerBuilderTest
    {
        private readonly PlayerBuilder PlayerBuilder;

        public PlayerBuilderTest()
        {
            PlayerBuilder = new PlayerBuilder();
        }

        [Fact]
        public void AgeCalcCheck()
        {
            var testBirthDay = new DateTime(2000, 10, 10); // test case birthday

            var testCase = 17; // test case age

            var expected = PlayerBuilder.GetAge(testBirthDay);

            Assert.Equal(testCase, expected);
        }
    }
}
