﻿using GuessingGame.Api.Builders;
using GuessingGame.Api.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using GuessingGame.Api.Custom;
using GuessingGame.Api.Enums;
using Xunit;

namespace GuessingGame.ApiTest
{
    public class LogFileBuilderTest
    {
        private readonly LogFileBuilder LogFileBuilder;

        public LogFileBuilderTest()
        {
            LogFileBuilder = new LogFileBuilder();
        }

        // test case logfile
        // test case playerId = 1
        // test case player log = 6 
        // 7 counts of playerId 1, however, one line has guesses(-1,-1,-1,-1),
        // indicating this is the playerlog initial entry, not an actual turn
        static readonly string testCasePlayerEntryCountLogfile = @"
                0	1	4	-1,-1,-1,-1	2018-05-07 13:24:09.559	1-1-1980	testplayer	7,9,9,3
                0	1	4	1,2,3,4	2018-05-07 13:24:14.405	1-1-1980	testplayer	7,9,9,3
                0	1	4	2,3,4,5	2018-05-07 13:24:23.245	1-1-1980	testplayer	7,9,9,3
                0	1	4	7,9,9,3	2018-05-07 13:24:36.374	1-1-1980	testplayer	7,9,9,3
                1	2	5	-1,-1,-1,-1,-1	2018-05-07 13:25:02.176	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	1,2,3,4,5	2018-05-07 13:25:06.740	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	1,1,2,3,5	2018-05-07 13:25:13.666	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	2,3,2,3,5	2018-05-07 13:25:24.581	1-1-1980	otherplayer	2,3,2,3,5
                2	3	5	-1,-1,-1,-1,-1	2018-05-07 13:28:55.121	1-1-1980	otherplayer	8,5,5,8,2
                2	3	5	1,2,3,4,5	2018-05-07 13:28:59.862	1-1-1980	otherplayer	8,5,5,8,2
                2	1	5	2,2,2,2,2	2018-05-07 13:29:05.691	1-1-1980	testplayer	8,5,5,8,2
                2	1	5	8,5,5,2,2	2018-05-07 13:29:20.773	1-1-1980	testplayer	8,5,5,8,2
                2	1	5	8,5,5,8,2	2018-05-07 13:29:27.712	1-1-1980	testplayer	8,5,5,8,2
            ";

        // test case for entry count with corrupted logfile
        // similar to testCasePlayerEntryCountLogfile, but missing a date of birth on the second line
        // 7 counts of playerId 1, however, one line has guesses(-1,-1,-1,-1),
        // indicating this is the playerlog initial entry, not an actual turn
        // minues one corrupted entry = 5
        static readonly string testCaseCorruptLogfile = @"
                0	1	4	-1,-1,-1,-1	2018-05-07 13:24:09.559	1-1-1980	testplayer	7,9,9,3
                0	1	4	1,2,3,4	2018-05-07 13:24:14.405 testplayer	7,9,9,3
                0	1	4	2,3,4,5	2018-05-07 13:24:23.245	1-1-1980	testplayer	7,9,9,3
                0	1	4	7,9,9,3	2018-05-07 13:24:36.374	1-1-1980	testplayer	7,9,9,3
                1	2	5	-1,-1,-1,-1,-1	2018-05-07 13:25:02.176	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	1,2,3,4,5	2018-05-07 13:25:06.740	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	1,1,2,3,5	2018-05-07 13:25:13.666	1-1-1980	otherplayer	2,3,2,3,5
                1	2	5	2,3,2,3,5	2018-05-07 13:25:24.581	1-1-1980	otherplayer	2,3,2,3,5
                2	3	5	-1,-1,-1,-1,-1	2018-05-07 13:28:55.121	1-1-1980	otherplayer	8,5,5,8,2
                2	3	5	1,2,3,4,5	2018-05-07 13:28:59.862	1-1-1980	otherplayer	8,5,5,8,2
                2	1	5	2,2,2,2,2	2018-05-07 13:29:05.691	1-1-1980	testplayer	8,5,5,8,2
                2	1	5	8,5,5,2,2	2018-05-07 13:29:20.773	1-1-1980	testplayer	8,5,5,8,2
                2	1	5	8,5,5,8,2	2018-05-07 13:29:27.712	1-1-1980	testplayer	8,5,5,8,2
            ";

        // options for logfilebuilder
        // playerId = 1
        private const long PlayerId = 1;
        private static readonly Dictionary<LogOptionsEnum, LogOptionsValues<long>> TestCaseDictionary = 
            new Dictionary<LogOptionsEnum, LogOptionsValues<long>>
        {
            [LogOptionsEnum.Player] = new LogOptionsValues<long> { PlayerId }
        };

        [Fact]
        public void TestGetPlayerLogFileEntriesCount()
        {
            const int expected = 6;
            var result = LogFileBuilder.BuildLogFile(testCasePlayerEntryCountLogfile, TestCaseDictionary).Items.Count;

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestGetCorruptLogFileEntriesCount()
        {
            const int expected = 5;
            var result = LogFileBuilder.BuildLogFile(testCaseCorruptLogfile, TestCaseDictionary).Items.Count;

            Assert.Equal(expected, result);
        }
    }
}
