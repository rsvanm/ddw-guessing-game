﻿using System;
using System.Collections.Generic;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Model.LogFile;
using GuessingGame.Api.Model.Player;
using GuessingGame.Api.Model.Statistics;
using Newtonsoft.Json;
using Xunit;

namespace GuessingGame.ApiTest
{
    public class StatisticsBuilderTest
    {
        private readonly StatisticsBuilder StatisticsBuilder;

        public StatisticsBuilderTest()
        {
            StatisticsBuilder = new StatisticsBuilder();
        }

        [Fact]
        public void TurnSpeedCalcTest()
        {
            var testDate = DateTime.Now;
            var testcase = new LogFile
            {
                Items = new List<LogFileItem>
                {
                    new LogFileItem
                    {
                        TimeStamp = testDate,
                        GameId = 1,
                    },
                    // testcase turn 1-2 = 50ms
                    new LogFileItem
                    {
                        TimeStamp = testDate.AddMilliseconds(50),
                        GameId = 1,
                    },
                    // testcase turn 2-3 = 100ms
                    new LogFileItem
                    {
                        TimeStamp = testDate.AddMilliseconds(150),
                        GameId = 1,
                    },
                    // testcase turn 3-4 = 50ms
                    new LogFileItem
                    {
                        TimeStamp = testDate.AddMilliseconds(200),
                        GameId = 1,
                    },
                }
            };

            // test case expcted values:
            // average turn time is (50+100+50)/3 = 66.67
            // maxturn time is 100ms
            // min turn time = 50ms
            // median turn time = 100ms
            var expected = new PlayerTurnSpeedStatistics
            {
                AvgTurnTime = Math.Round((50f + 100 + 50) / 3, 2),
                MaxTurnTime = 100f,
                MinTurnTime = 50f,
                MedianTurnTime = 100f,
                PlayerId = 0, // default
            };

            var result = StatisticsBuilder.BuildPlayerTurnSpeedStatistics(testcase);

            Assert.Equal(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(result));
        }
    }
}
