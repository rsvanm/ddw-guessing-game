﻿using System;
using System.Collections.Generic;
using System.Text;
using GuessingGame.Api.Builders;
using GuessingGame.Api.Model.Game;
using GuessingGame.Api.Model.LogFile;
using Newtonsoft.Json;
using Xunit;

namespace GuessingGame.ApiTest
{
    public class GameTurnSubmissionTest
    {
        private readonly GameTurnBuilder GameTurnBuilder;

        public GameTurnSubmissionTest()
        {
            GameTurnBuilder = new GameTurnBuilder();
        }

        [Fact]
        public void TestGameWon()
        {
            var testLogFile = new LogFile
            {
                Items = new List<LogFileItem>
                {
                    new LogFileItem
                    {
                        GameId = 0, // default
                        PlayerId = 0, // default
                        NumSize = 4, // default
                        Guesses = new List<long>
                        {
                            1, 2, 3, 4, // test case Guesses
                        },
                        TimeStamp = DateTime.Now, // default
                        DateOfBirth = new DateTime(2000, 01, 01), // default
                        Name = "test", // default
                        RequiredOutcome = new List<long>
                        {
                            1, 2, 3, 4, // test case required outcome
                        },
                    }                   
                }
            };

            var testSubmission = new GameTurnSubmission
            {
                GameId = 0,
                Guesses = new List<long>
                {
                    1, 2, 3, 4, // test case guess submission
                },
            };

            var expected = new GameTurn
            {
                CorrectGuesses = 4, // all correct
                GameId = 0, // default
                NumSize = 4, // amount of numbers
                PlayerId = 0, // default
                TurnNumber = 1, // only one logitem passed to builder
            };

            var result = GameTurnBuilder.BuildGameTurn(testLogFile, testSubmission);

            Assert.Equal(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(result));
        }
    }
}
